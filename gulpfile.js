var gulp = require('gulp');
var source = require('vinyl-source-stream'); // Used to stream bundle for further handling
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify'); 
var concat = require('gulp-concat');
var gutil = require('gulp-util');

gulp.task('browserify', function(error_callback) {
    var bundler = browserify({
        entries: ['./js/index.js'], // Only need initial file, browserify finds the deps
        transform: [reactify], // We want to convert JSX to normal javascript
        debug: true, // Gives us sourcemapping
        cache: {}, packageCache: {}, fullPaths: true // Requirement of watchify
    });
    var watcher  = watchify(bundler);

    return watcher
    .bundle().on('error', gutil.log) // Create the initial bundle when starting the task
    .pipe(source('js/index.js'))
    .pipe(gulp.dest('build/'));

    error_callback(err);
});

gulp.task('copy-web', ['browserify'], function() {
    return gulp.src('index.html').pipe(gulp.dest('build/'))
});

gulp.task('copy-css', ['browserify'], function() {
    return gulp.src('css/**/*').pipe(gulp.dest('build/css'))
});


gulp.task('deploy-web', ['copy-web', 'copy-css'], function() {
    return gulp.src('build/**/*').pipe(gulp.dest('./server/app/'))
});

gulp.task('deploy-electron', function() {
    gulp.src('build/css/**/*').pipe(gulp.dest('../electron/app/css'))
    gulp.src('js/**/*').pipe(gulp.dest('../electron/app/js'))
    gulp.src('index.html').pipe(gulp.dest('../electron/app/'))
});


// Just running the two tasks
gulp.task('default', ['browserify', 'deploy-web']);
gulp.watch(['index.html', 'js/**/*', 'css/**/*'], ['default']);
