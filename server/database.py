import pandas as pd
import numpy as np
from sqlalchemy import ForeignKey, Text, Float, Integer, DateTime, Boolean, Column, create_engine, Column, Table, String
from sqlalchemy.orm import relationship, backref
from sqlalchemy.sql import text
from flask.ext.security import RoleMixin, UserMixin
import sqlalchemy.dialects.postgresql as postgresql

from datetime import timedelta, datetime

from sqlalchemy.ext.declarative import declarative_base
from common import db

Base = declarative_base()

# Role class
class Role(db.Model, RoleMixin):
    __tablename__ = 'role'
    # Our Role has three fields, ID, name and description
    id = Column(Integer, primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255))

    # __str__ is required by Flask-Admin, so we can have human-readable values for the Role when editing a User.
    # If we were using Python 2.7, this would be __unicode__ instead.
    def __str__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Role' when saving a User
    def __hash__(self):
        return hash(self.name)


# User class
class User(db.Model, UserMixin):
    __tablename__ = 'user'
    # Our User has six fields: ID, email, password, active, confirmed_at and roles. The roles field represents a
    # many-to-many relationship using the roles_users table. Each user may have no role, one role, or multiple roles.
    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    password = Column(String(255))
    active = Column(Boolean())
    confirmed_at = Column(DateTime())
    roles = relationship(
        'Role',
        secondary="roles_users",
        backref=backref('users', lazy='dynamic')
    )

    def __str__(self):
        return self.email

class RoleUser(db.Model):
    __tablename__ = "roles_users"
    id = Column(Integer, primary_key=True)
    role_id = Column(Integer, ForeignKey('role.id'))
    role = relationship("Role")
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User")

    def __str__(self):
        return "User({0}) for {1}".format(user, role)


