from flask import Flask, jsonify, render_template, abort, Response, make_response

from sqlalchemy import text
from sqlalchemy.event import listens_for

from flask.ext.security import current_user, login_required, RoleMixin, Security, SQLAlchemyUserDatastore, UserMixin, utils
from flask.ext.admin import Admin, form
from flask.ext.admin.contrib import sqla

from flask_sqlalchemy import SQLAlchemy

import pandas
import os
import os.path as op
import io
import csv
import json
import appconfig

from wtforms.fields import PasswordField
from database import User, Role, RoleUser

from common import db

app = Flask(__name__, static_folder='app', template_folder='app', static_url_path='')

app.config['SECRET_KEY'] = appconfig.KEY 

os.environ['DATABASE_URL'] = appconfig.DATABASE_URL 

app.config['SQLALCHEMY_DATABASE_URI'] = appconfig.DATABASE_URL 
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
app.config['SECURITY_PASSWORD_HASH'] = 'pbkdf2_sha512'
#this is a 'double salt', pbkdf2_sha512 generates a per password salt already
app.config['SECURITY_PASSWORD_SALT'] = appconfig.SALT 

db.init_app(app)

# Initialize the SQLAlchemy data store and Flask-Security.
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

@app.before_first_request
def initialize():

    # Create any database tables that don't exist yet.
    db.create_all()

    # Create the Roles "admin" and "end-user" -- unless they already exist
    user_datastore.find_or_create_role(name='admin', description='Administrator')
    user_datastore.find_or_create_role(name='end-user', description='End user')

    # Create two Users for testing purposes -- unless they already exists.
    # In each case, use Flask-Security utility function to encrypt the password.
    encrypted_password = utils.encrypt_password(appconfig.ADMIN_PW)
    if not user_datastore.get_user('someone@null.edu'):
        user_datastore.create_user(email='someone@null.edu', password=encrypted_password)
    if not user_datastore.get_user('admin@null.edu'):
        user_datastore.create_user(email='admin@null.edu', password=encrypted_password)

    # Commit any database changes; the User and Roles must exist before we can add a Role to the User
    #db.session.commit()

    # Give one User has the "end-user" role, while the other has the "admin" role. (This will have no effect if the
    # Users already have these Roles.) Again, commit any database changes.
    user_datastore.add_role_to_user('someone@null.edu', 'end-user')
    user_datastore.add_role_to_user('admin@null.edu', 'admin')
    db.session.commit()

@app.route('/')
@login_required
def index():
    return render_template('index.html')



# Customized User model for SQL-Admin
class UserAdmin(sqla.ModelView):

    # Don't display the password on the list of Users
    column_exclude_list = ('password',)

    # Don't include the standard password field when creating or editing a User (but see below)
    form_excluded_columns = ('password',)

    # Automatically display human-readable names for the current and available Roles when creating or editing a User
    column_auto_select_related = True

    # Prevent administration of Users unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return current_user.has_role('admin')

    # On the form for creating or editing a User, don't display a field corresponding to the model's password field.
    # There are two reasons for this. First, we want to encrypt the password before storing in the database. Second,
    # we want to use a password field (with the input masked) rather than a regular text field.
    def scaffold_form(self):

        # Start with the standard form as provided by Flask-Admin. We've already told Flask-Admin to exclude the
        # password field from this form.
        form_class = super(UserAdmin, self).scaffold_form()

        # Add a password field, naming it "password2" and labeling it "New Password".
        form_class.password2 = PasswordField('New Password')
        return form_class

    # This callback executes when the user saves changes to a newly-created or edited User -- before the changes are
    # committed to the database.
    def on_model_change(self, form, model, is_created):

        # If the password field isn't blank...
        if len(model.password2):

            # ... then encrypt the new password prior to storing it in the database. If the password field is blank,
            # the existing password in the database will be retained.
            model.password = utils.encrypt_password(model.password2)


# Customized Role model for SQL-Admin
class RoleAdmin(sqla.ModelView):

    # Prevent administration of Roles unless the currently logged-in user has the "admin" role
    def is_accessible(self):
        return current_user.has_role('admin')

# Initialize Flask-Admin
admin = Admin(app)

# Add Flask-Admin views for Users and Roles
admin.add_view(UserAdmin(User, db.session))
admin.add_view(RoleAdmin(Role, db.session))
#admin.add_view(UserRoleAdmin(RoleUser, db.session))

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)

